# Ansible Role: Zentyal

This role installs [Zentyal](https://zentyal.com/) package on any supported host.

## Requirements

None

## Installing

The role can be installed by running the following command:

```bash
git clone https://gitlab.com/enmanuelmoreira/ansible-role-zentyal enmanuelmoreira.zentyal
```

Add the following line into your `ansible.cfg` file:

```bash
[defaults]
role_path = ../
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    zentyal_version: 7.0
    zentyal_gpgkey_path: http://keys.zentyal.org/zentyal-{{ zentyal_version }}-packages.asc
    zentyal_repo_path: deb http://packages.zentyal.org/zentyal {{ zentyal_version }} main extra
    zentyal_user: zentyal
    zentyal_password: zentyal

The version of the metapackage:

    zentyal_version: 7.0

The path where the gpgkey of the repository will be downloaded.

    zentyal_gpgkey_path: http://keys.zentyal.org/zentyal-{{ zentyal_version }}-packages.asc

The path of the repository of Zentyal:

    zentyal_repo_path: deb http://packages.zentyal.org/zentyal {{ zentyal_version }} main extra

The administrative user and password of the web interface:
    
    zentyal_user: zentyal
    zentyal_password: zentyal

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: enmanuelmoreira.zentyal

## License

MIT / BSD
